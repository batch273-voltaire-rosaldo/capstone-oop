// Capstone OOP

// Class Customer
class Customer {
	constructor(email, cart) {
		this.email = email;
		this.cart = cart;
		this.orders = [];

	}

    checkOut() {
        return this.orders.map(order => {
            return order.checkOut();
        })
    }

}

//Class Product
class Product {
    constructor(name, price) {
        this.name = name;
        this.price = price;
        this.isActive = true;

    }

    archive() {
        this.isActive = false;
    }

    updatePrice(price) {
        this.price = price;
    }
}

//Class Cart
class Cart {
	constructor() {
		this.contents = [];
		this.totalAmount = 0;

	}

    addToCart(name, qty) {
        // this.contents.push({
        this.contents.push(new Product(name, qty));
    }

    showCartContents() {
        this.contents.forEach(item => {
            console.log(item.name + " " + item.price);
        })
    }

    updateProductQuantity() {
        this.contents.forEach(item => {
            item.updatePrice();
        })
    }

    clearCartContents() {
        this.contents = [];
    }

    computeTotal() {
        this.totalAmount = this.contents.reduce((total, item) => {
            return total + item.price;
        })
    }

}

const john = new Customer('john@email.com');
const jane = new Customer('jane@email.com');

const prodA = new Product('soap', 9.50);
const prodB = new Product('shampoo', 7.50);